import { createApp } from 'vue'
import { createPinia } from 'pinia'
import './style.css'
import "ant-design-vue/dist/reset.css";
import dayjs from 'dayjs'
import App from './App.vue'
import router from './router'
import Antd from "ant-design-vue";
const pinia = createPinia()
const app = createApp(App)
app.provide('dayJS', dayjs)
app.use(router)
app.use(Antd)
app.use(pinia)
app.mount('#app') 