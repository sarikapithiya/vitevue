import { createRouter, createWebHistory } from 'vue-router';
import Dashboard from '../pages/Dashboard/index.vue';
import Patients from '../pages/patients/index.vue';
import caseHistory from '../pages/case-history/index.vue';
import payments  from '../pages/payments/index.vue';
import users from '../pages/users/index.vue';
import contacts from '../pages/contacts/index.vue';
import announcements from '../pages/announcements/index.vue';
import labReport from '../pages/lab-reports/index.vue'

const routes = [
    {
        path: '/',
        component: Dashboard,
        name:'dashboard',
        meta: { title: 'Dashboard' },
    },
    {
        path: '/Patients',
        component: Patients,
        meta: { title: 'Patients' },
    },
    {
        path: '/case-history',
        component: caseHistory,
        meta: { title: 'Case History' },
    },
    {
        path: '/payments',
        component: payments,
        meta: { title: 'Payments' },
    },
    {
        path: '/users',
        component: users,
        meta: { title: 'Users' },
    },
    {
        path: '/contacts',
        component: contacts,
        meta: { title: 'Contacts' },
    },
    {
        path: '/announcements',
        component: announcements,
        meta: { title: 'Announcements' },
    },
    {
        path: '/lab-reports',
        component: labReport,
        meta: { title: 'Lab Reports' },
    },
    
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
