import { defineStore } from "pinia";
import { computed, ref } from "vue";

export const useDrawerStore = defineStore("drawer", () => {
  const sideDrawer = ref(false);
  const title = ref("Drawer Title");
  const width = ref("50%");
  const component = ref("");
  const placement = ref("right");
  const loading = ref(false);
  const callback = ref();
  const payload = ref({});

  // GETTERS
  const getSideDrawerStatus = computed(() => sideDrawer.value);
  const getTitle = computed(() => title.value);
  const getWidth = computed(() => width.value);
  const getComponent = computed(() => component.value);
  const getPlacement = computed(() => placement.value);
  const getLoading = computed(() => loading.value);
  const getCallback = computed(() => callback.value);
  const getPayload = computed(() => payload.value);

  // SETTERS
  const setSideDrawerStatus = (value) => (sideDrawer.value = value);
  const setTitle = (value) => (title.value = value);
  const setWidth = (value) => (width.value = value);
  const setComponent = (value) => (component.value = value);
  const setPlacement = (value) => (placement.value = value);
  const setLoading = (value) => (loading.value = value);
  const setCallback = (value) => (callback.value = value);
  const setPayload = (value) => (payload.value = value);

  // ACTIONS
  const open = ({ title, width, component, placement, callback, payload }) => {
    setSideDrawerStatus(true);
    setTitle(title);
    setWidth(width ?? "50%");
    setComponent(component);
    setPlacement(placement ?? "right");
    setCallback(callback);
    setPayload(payload);
  };

  const reset = () => {
    setSideDrawerStatus(false);
    setTitle("Drawer Title");
    setWidth("50%");
    setPlacement("right");
    setCallback(() => {});
    setPayload({});
    setComponent(null);
    setLoading(false);
  };

  const loadingStart = () => setLoading(true);
  const loadingStop = () => setLoading(false);

  return {
    // ACTIONS
    open,
    reset,
    loadingStart,
    loadingStop,

    //  GETTERS
    getSideDrawerStatus,
    getTitle,
    getWidth,
    getComponent,
    getPlacement,
    getLoading,
    getCallback,
    getPayload,
  };
});
