import dayjs from "dayjs";

export const DDMMYYYY = "DD/MM/YYYY";
export const MMMMDDYYYY = "MMMM DD, YYYY";
export const YYYYMMDD = "YYYY-MM-DD";

export const disableFutureDate = (current) => {
  return current && current > dayjs().endOf("day");
};

// export const dis
